# Wimsey Race Results 
## Wed Mar 27 02:54:00 PM PDT 2024

## Overview

The Race Results website is hosted as an *AWS S3 Bucket* configured as a *static website*.

Results are organized in a hierarchy based on:

1. Year - e.g. 2024
2. Organizer - e.g. Thrashers
3. Series - e.g. Spring Series
4. Results and document files

The website contains an *index.html* file that contains the *Javascript* to display the hierarchy,
allow the website visitor to navigate to a specific series, and then show the results and documents
related to that series.

## Results

Unless otherwise specified files are deemed to be results if the follow this pattern:

`YYYY-MM-DD Event Name .[html|pdf|md]`

Multiple events each with multiple files are allowed and presented in a concise and simple manner to the viewer.

## Other files

Other files can be shown in separate areas of the displayed page. 

The categories are:

- COMMUNIQUE - official communiques from race officials
- PRESS - press reports
- SERIES - series information
- START - start lists
- OTHER - other
- DOC - documents

To have a file displayed in one of these categories the Javascript code needs to have some information. 
This is provided using a zero length dotfile of the form:

`.CATEGORY-pattern`

Where pattern matches some portion of the uploaded file.

E.g.:

`.DOC-Tech` to match `Bradner Long TechGuide.pdf`

## Exclude

In some cases it may be necessary to exclude some files or directories. 

	- EXCLUDE-
	- EXCLUDEDIR

