
The World Tuesday Night Championships (WTNC) continues a long-standing tradition of weekly hammerfests that stretches back to the 1980s. 
Points are awarded for each race in the series and redeemed for cash prizing at the end of the series. Leaders jerseys are issued to interim and final series leaders.

Alternating between the 
[Glenlyon](http://ridewithgps.com/routes/2377250) and 
[UBC](http://ridewithgps.com/routes/395772) courses on fair-weather Tuesday evenings.

For more information see [Escape Velocity WTNC homepage](https://escapevelocity.bc.ca/wtnc "WTNC Homepage")


