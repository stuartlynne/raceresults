![Escape Velocity](https://results.wimsey.co/logo-ev-hdr-1.png)
# BC Race Results 
### Sponsored by Escape Velocity

*Copyright (c) 2018-2019 Stuart Lynne*

This website contains archived race results for cycling road races in BC.

Comments and suggestions stuart.lynne@gmail.com

If you have any questions about any races contact the above with the following information:

1. The exact URL to the results your question is about.
2. The Riders first and last name.
3. The category and bib number of the rider.
4. A description of the problem.

