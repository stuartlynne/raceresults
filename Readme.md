# S3 Race Results 
## Copyright (c) 2018-2024 Stuart.Lynne@gmail.com
## Sat Nov 24 12:52:45 PST 2018


### S3 Bucket Static Website
This describes how we use an AWS S3 Bucket Static Website to store Race Results.

S3 Buckets configured as a static website provide a very low cost, low maintenance,
resilient storage area for files that can be retrieved and viewed with a web browser.

S3 static websites have the following characteristics:

    - do not support directory listings
    - do not support any server side scripting
    - support a default document, e.g. index.html
    - will redirect to an error document
    - api calls to search for matching keys (i.e. sub-directories) are inexpensive and fast

We leverage the last two characteristics by setting both the default document
and the error document to be the index.html file.

The intent is to have a storage repository that can be simply accessed but 
requires little or no maintenance. Just get the files uploaded correctly and
then leave them there.


### Hierarchical Database CMS

The S3 filesystem is being as a Hierarchical Database CMS (Content Management System):

    - A naming hierarchy is used to build a hierarchical database. 
    - Meta data is stored either in a file name or a special file.
    - Objects in the CMS are files. 

For example:
    - /2019/wtnc2019/ - CMS directory node, where to put information about wtnc 2019 series
    - /2019/wtnc2019/2019-06-11-WTNC-Glenlyon.photos-tlbrimner - meta data in file name, where to find photos for specific day
    - /2019/wtnc2019/sponsors.json - meta data in file, list of sponsor logos for wtnc 2019
    - /2019/wtnc2019/2019-06-18-WTNC\ Glenlyon-730-r2.html - race result file

The photo link is an example showing how to store some special data in the file name. 
This is used to find the actual photos which are stored in a separate hierarchy in the database.

The sponsors.json is an example of a special file name being used to store information that can be used when generating an html page for viewing.
In this case, a list of sponsor logos that will be shown in the sponsor logo part of the generated page.


### Content
The results website needs to store content of various types:
    - race results
    - series results
    - press releases
    - sponsor information
    - photos

We can break the above into:
    - race results, series results, which are generated and automatically uploaded 
    - supplementary documents such as start lists, technical guides, which are manually uploaded
    - photos which are matched to a specific race (or day of racing) and automatically uploaded
    - editorial content
    - sponsor information and logos


The intent is to organize results by organizer and/or series. 


### /index.html

The S3 website implementation does not support any processing on the server. All
formatting etc is done in the browser. 

The */index.html* file contains javascript to fetch the S3 contents of any requested
directory and format it as a directory listing. It will also redirect upwards
if a non-existent or empty directory is specified.

This index.html utilizes error redirection in two ways.

    1. The S3 bucket static website and Cloudfront are configured to return /index.html
    for any errors. I.e. attempts to load something that does not exist.

    2. The index.thml file itself will attempt to find the closest valid path corresponding
    to what it was provided with, redirecting until it as something valid.


### S3 file hierarchy

This file is the root index.html for an S3 bucket configured as a static website containing
race results. The raceresults file heirarchy on the S3 bucket look like:

     /
     /index.html
     /2018/
     /2018/lmcx2018/
     /2018/lmcx2018/someresults.html
     /2018/lmcx2018/someresults.pdf

Access to any existing result file (i.e. html or pdf file) will return the requested file.

Access to anything else will result in a redirect to the */index.html* file.

The redirection attempts to find the closest directory for the user. So /2018/a/b/c will
redirect first to /2018/a/b/c/, then /2018/a/b/, then /2018/a/, then /2018/ and finally /.
The first existing directory will be displayed. The top level directory is auto-generated
for the years from the current year back to 2013.

The job of the index.html file is to determine what directory the user is attempting to look
at and return a directory listing for that directory by loading the fetching the files
in the directory using an S3 query:

    https://wimseyraceresults.s3-us-west-2.amazonaws.com/?delimiter=/&prefix=

formatting it to display to the requester.

The new S3 access means that simply requesting a directory will result in a redirect which
will be redirected and index html must look at the pathname to see what was requested.

To support the legacy usage which was an index.cgi script, the index.html file will also
support looking at a path sent as an form parameter like ?path=2018/lmcx2018.


Best practice is to generate race results in a standalone file such as html, pdf or md (markdown)
formats. These are stored in a file hierarchy like:

    /year/seriesname/racename.extension

If you have a set of files on the results computer a simple s3cmd can be used
to sync the files to the website.

For example if we had the following files:

    /lmcx2018/
    /lmcx2018/someresults.html
    /lmcx2018/someresults.pdf

Then the following would be sufficient to get them into the S3 Bucket:

    s3cmd sync lmcx2018 s3://wimseyraceresults/2018/

### URLS

URL's can look like the following.

href legacy query style:

>       ?path=./2018/lmcx2018

Pathname:
>     /                       # valid
>     /2018                   # invalid, does not end in /, append / and redirect
>     /2018/                  # valid year
>     /2018/lmxc2018/         # valid year / series tag
>     /2018/lmxc2018          # invalid, does not end in /, append / and redirect
>     /2018/lmxc2018/xxx/     # invalid, does not exist, redirect to /2018/lmcx2018/

The invalid URL's will have a / added and then redirected to.

If a valid URL does not exist, then the last directory is removed and the result is
redirected to. E.g.:

>   /2018/a/b -> /2018/a/b/
>   /2018/a/b/ -> /2018/a/
>   /2018/a/ -> /2018/


### Display 

The */index.html* file effectively replaces the directory listing feature in
normal web servers, e.g. *Apache2*.

The directory listing feature is enhanced to suit the purpose. Separate
areas in the screen are used to display:

    sub-directories
    series results
    race results
    other results
    documents
 
Also, a Title and Credits file (in markdown format) can be supplied to
provide some additional information that will be displayed.

The result file listsing are displayed to the user in the following order:

    title           - display Title.md if available
    search          - Google Search engine
    back            - back navigation 
    directories     - sub-directories if available
    race results    - race results
    press releases  - press releases
    series results  - series results if available
    other results   - other results if available
    documents       - documents
    credits         - display Credits.md if available
    sponsors        - display sponsor logos if available
    quote           - display a cycling related quote

Directories are automatically recognized. All files will be displayed as race results
unless otherwise identified.

### Special files

Series result files, other result files, and documents are matched using a special dot file
to specify a match for them. The format for these is simple:

    .SERIES-xxxx
    .OTHER-xxxx
    .DOC-xxxx

Where xxxx is a unique string that will match one of more files of the type that should
be displayed in this section.

These files are empty, the information needed by index.html for sorting is contained
in the name. The file names are automatically returned as part of the directory listing
at minimal additional cost. 


### Sample
E.g. the contents of 2018/wtnc2018/:

    .OTHER-snoball
    .SERIES-series
    .SERIES-upgrade
    .DOC-tech
    Credit.md
    Title.md
    2018-04-24-WTNC-Glenlyone-620-41.html
    2018-04-24-WTNC-Glenlyone-620-41.pdf
    2018-wtnc-series.html
    2018-wtnc-upgrade-html
    m12-snoball.html
    wtnc-tech.pdf



### AWS Setup  

N.B. AWS must be setup as follows:

1. The S3 bucket name MUST NOT have any dots in it. Dots will make https access to the S3 API
from index.html show as insecure in Chrome.

2. The S3 bucket must be setup as a static website, with error redirect for 404 set to /index.html

3. The Cloudfront distribution must have the default root object set to /index.html and the error
redirect for 404 set to /index.html.

4. An Amazon certificate must be requested for the hosting domain and pointed at the Cloudfront
distribution domain name.


### Favicon
https://www.ionos.com/tools/favicon-generator

